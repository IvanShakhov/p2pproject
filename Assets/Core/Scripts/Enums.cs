namespace Core
{
    public enum EnumNetworkEvents
    {
        None,
        RoomCreate,
        RoomConnect,
        RoomStart,
        RoomEnd,
        RoomUpdate
    }

    public enum EnumSceneName
    {
        None,
        Loading,
        Lobby,
        Game
    }

}