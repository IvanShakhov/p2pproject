using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{
    [field:SerializeField] private List<Scene> Scenes { get; set; }
    [field:SerializeField] private ViewLoading ViewLoading { get; set; }

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        ViewLoading = FindObjectOfType<ViewLoading>();
        StartCoroutine(RoutineLoadScene(EnumSceneName.Lobby));
    }

    private IEnumerator RoutineLoadScene(EnumSceneName sceneName)
    {
        yield return new WaitForNextFrameUnit();
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName.ToString());
        //asyncLoad.allowSceneActivation = false;
        
        while (!asyncLoad.isDone)
        {
            ViewLoading.UpdateView(asyncLoad.progress);
            yield return new WaitForNextFrameUnit();
        }
        //asyncLoad.allowSceneActivation = true;

    }
}
