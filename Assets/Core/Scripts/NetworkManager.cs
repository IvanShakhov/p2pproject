using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

namespace Core.Network
{
    public class NetworkManager : MonoBehaviour
    {
        
        [field: SerializeField] private SocketIOComponent _socket { get; set; }
        //private ManagerChat _managerChat;
        void Start()
        {
            Init();
        }

        private void Init()
        {
            //_managerChat = FindObjectOfType<ManagerChat>();
            _socket.On(EnumNetworkEvents.RoomCreate, OnRoomCreate);
            _socket.On(EnumNetworkEvents.RoomConnect, OnRoomConnect);
            _socket.On(EnumNetworkEvents.RoomStart, OnRoomStart);
            _socket.On(EnumNetworkEvents.RoomEnd, OnRoomEnd);
            _socket.On(EnumNetworkEvents.RoomUpdate, OnRoomUpdate);
        }

        // private void OnChatMessage(SocketIOEvent e)
        // {
        //     Debug.Log("OnChatMessage data пришла === " + e.data);
        //     SchemaOnChatMessage tempMessage= JsonUtility.FromJson<SchemaOnChatMessage>(e.data.ToString());
        //     Debug.Log("OnChatMessage end");
        //     //Добавить сообщение в чат
        //     _managerChat.AddMessage(tempMessage);
        //
        // }
        // private void OnLoad(SocketIOEvent e)
        // {
        //     Debug.Log("OnLoad data пришла === " + e.data);
        //     SchemaLoad tempMessage= JsonUtility.FromJson<SchemaLoad>(e.data.ToString());
        //     Debug.Log("OnLoad end");
        //     ManagerData.instance.LoadGame(tempMessage);
        // }

        
        private void OnRoomCreate(SocketIOEvent message)
        {
            
        }
        private void OnRoomConnect(SocketIOEvent message)
        {
            
        }
        private void OnRoomStart(SocketIOEvent message)
        {
            
        }
        private void OnRoomEnd(SocketIOEvent message)
        {
            
        }
        private void OnRoomUpdate(SocketIOEvent message)
        {
            
        }
        
        public void Send(EnumNetworkEvents networkEvent, object message)
        {
            _socket.Emit(Enum.GetName(typeof(EnumNetworkEvents), networkEvent),
                new JSONObject(JsonUtility.ToJson(message)));
        }
    }
}
