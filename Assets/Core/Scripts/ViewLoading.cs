using UnityEngine;
using UnityEngine.UI;

public class ViewLoading : MonoBehaviour
{
    [field: SerializeField] private Image ImageProgress { get; set; }

    public void UpdateView(float value)
    {
        ImageProgress.fillAmount = value + 0.1f;
    }
}
