using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalObject : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
