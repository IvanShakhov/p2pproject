using System.Collections.Generic;
using UnityEngine;
using System;

namespace Core.Network
{
    [Serializable]
    public class SchemaRoomCreate
    {
        public SchemaRoom schemaRoom;
    }
    [Serializable]
    public class SchemaRoomConnect
    {

    }
    [Serializable]
    public class SchemaRoomStart
    {

    }
    [Serializable]
    public class SchemaRoomEnd
    {

    }
    [Serializable]
    public class SchemaRoomUpdate
    {

    }

    [Serializable]
    public class SchemaRoom
    {
        public string RoomdId;
        public string RoomName;
        public string HostId;
        public int RoomSize;
        public List<SchemaPlayer> RoomPlayers;
    }
    [Serializable]
    public class SchemaPlayer
    {
        public string PlayerName;
        public float PlayerMaxHealth;
        public float PlayerCurrentHealth;
        public int PlayerCoins;
        public Vector3 PlayerPosition;
        public Vector3 PlayerRotation;
        public string PlayerIcon;
        public string PlayerColor;
    }
    [Serializable]
    public class SchemaBullet
    {
        public float BulletDamage;
        public Vector3 BulletStartPosition;
        public Vector3 BulletDirection;
        public float BulletSpeed;
    }
}
